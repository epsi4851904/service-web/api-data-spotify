Pour envoyer les data depuis un json à une bdd il faut :

parse le json, 
playlist -> music( artist_name, track_uri, artist_uri etc ...)

verifier si la playlist existe :
    oui : ->
    non : créer la playlist ->
    verifier si la musique existe
        oui : lier la position de la musique avec la playlist
        non : créer la musique, lié la musique avec la playlist, 
        vérifier si l'album existe
            oui : -> next
            non : créer l'album -> next
            vérifier si la musique fait partie de l'album
                oui : -> exit
                non : -> ajouter la musique à l'album
                verifier si l'artiste existe :
                    oui : lié l'album avec l'artiste
                    non : créer l'artiste, lié l'album avec l'artiste
                        
