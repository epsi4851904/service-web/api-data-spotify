
import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { Music_Playlist, Prisma } from '@prisma/client';

@Injectable()
export class Music_PlaylistService {
  constructor(private prisma: PrismaService) {}

  async music_playlist(
    music_playlistWhereUniqueInput: Prisma.Music_PlaylistWhereUniqueInput,
  ): Promise<Music_Playlist | null> {
    return this.prisma.music_Playlist.findUnique({
      where: music_playlistWhereUniqueInput,
    });
  }

  async music_playlists(params: {
    skip?: number;
    take?: number;
    cursor?: Prisma.Music_PlaylistWhereUniqueInput;
    where?: Prisma.Music_PlaylistWhereInput;
    orderBy?: Prisma.Music_PlaylistOrderByWithRelationInput;
  }): Promise<Music_Playlist[]> {
    const { skip, take, cursor, where, orderBy } = params;
    return this.prisma.music_Playlist.findMany({
      skip,
      take,
      cursor,
      where,
      orderBy,
    });
  }

  async countMusic_Playlists(where: Prisma.Music_PlaylistWhereInput): Promise<number> {
    return this.prisma.music_Playlist.count({ where });
  }
  
}
