import { Module } from '@nestjs/common';
import { Music_PlaylistService } from './music_playlist.service';
import { PrismaModule } from '../prisma/prisma.module';

@Module({
  imports: [PrismaModule],
  providers: [Music_PlaylistService],
  exports: [Music_PlaylistService],
})
export class MusicPlaylistModule {}