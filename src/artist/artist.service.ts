
import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { Artist, Prisma } from '@prisma/client';

@Injectable()
export class ArtistService {
  constructor(private prisma: PrismaService) {}

  async artist(
    artistWhereUniqueInput: Prisma.ArtistWhereUniqueInput,
  ): Promise<Artist | null> {
    return this.prisma.artist.findUnique({
      where: artistWhereUniqueInput,
    });
  }

  async artists(params: {
    skip?: number;
    take?: number;
    cursor?: Prisma.ArtistWhereUniqueInput;
    where?: Prisma.ArtistWhereInput;
    orderBy?: Prisma.ArtistOrderByWithRelationInput;
  }): Promise<Artist[]> {
    const { skip, take, cursor, where, orderBy } = params;
    return this.prisma.artist.findMany({
      skip,
      take,
      cursor,
      where,
      orderBy,
    });
  }

  async countArtists(where: Prisma.ArtistWhereInput): Promise<number> {
    return this.prisma.artist.count({ where });
  }
  
}
