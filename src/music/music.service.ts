
import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { Music, Prisma } from '@prisma/client';

@Injectable()
export class MusicService {
  constructor(private prisma: PrismaService) {}

  async music(
    musicWhereUniqueInput: Prisma.MusicWhereUniqueInput,
  ): Promise<Music | null> {
    return this.prisma.music.findUnique({
      where: musicWhereUniqueInput,
    });
  }

  async musics(params: {
    skip?: number;
    take?: number;
    cursor?: Prisma.MusicWhereUniqueInput;
    where?: Prisma.MusicWhereInput;
    orderBy?: Prisma.MusicOrderByWithRelationInput;
  }): Promise<Music[]> {
    const { skip, take, cursor, where, orderBy } = params;
    return this.prisma.music.findMany({
      skip,
      take,
      cursor,
      where,
      orderBy,
    });
  }

  async countMusics(where: Prisma.MusicWhereInput): Promise<number> {
    return this.prisma.music.count({ where });
  }
  
}
