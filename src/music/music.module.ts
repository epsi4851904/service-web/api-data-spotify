import { Module } from '@nestjs/common';
import { MusicService } from './music.service';
import { PrismaModule } from '../prisma/prisma.module';

@Module({
  imports: [PrismaModule],
  providers: [MusicService],
  exports: [MusicService],
})
export class MusicModule {}