
import {
  Controller,
  Get,
  Param,
  Query,
} from '@nestjs/common';
import { ApiTags, ApiParam, ApiOperation, ApiQuery } from '@nestjs/swagger';
import { AlbumService } from './album/album.service';
import { ArtistService } from './artist/artist.service';
import { MusicService } from './music/music.service';
import { Music_PlaylistService } from './music_playlist/music_playlist.service';
import { PlaylistService } from './playlist/playlist.service';
import { Artist as ArtistModel, Album as AlbumModel, Music as MusicModel, Playlist as PlaylistModel, Music_Playlist as Music_PlaylistModel } from '@prisma/client';

@Controller()
export class AppController {
  constructor(
    private readonly albumService: AlbumService,
    private readonly artistService: ArtistService,
    private readonly musicService: MusicService,
    private readonly music_playlistService: Music_PlaylistService,
    private readonly playlistService: PlaylistService,
  ) {}

  @Get('artist/:id')
  async getArtistById(@Param('id') id: string): Promise<ArtistModel> {
    return this.artistService.artist({ artist_uri: String(id) });
  }

  @Get('artists')
  @ApiOperation({ summary: 'Get all artists' })
  @ApiQuery({ name: 'take', required: false, description: 'Number of artists to return', type: Number })
  @ApiQuery({ name: 'skip', required: false, description: 'Number of artists to skip', type: Number })
  async getArtists(
    @Query('take') take?: string,
    @Query('skip') skip?: string,
  ): Promise<ArtistModel[]> {
    const parsedTake = take ? parseInt(take, 10) : undefined;
    const parsedSkip = skip ? parseInt(skip, 10) : undefined;
    return this.artistService.artists({ take: parsedTake, skip: parsedSkip });
  }

  @Get('artists/count')
  async countArtists(): Promise<number> {
    return this.artistService.countArtists({});
  }

  @Get('album/:id')
  async getAlbumById(@Param('id') id: string): Promise<AlbumModel> {
    return this.albumService.album({ album_uri: String(id) });
  }

  @Get('albums')
  @ApiOperation({ summary: 'Get all albums' })
  @ApiQuery({ name: 'take', required: false, description: 'Number of albums to return', type: Number })
  @ApiQuery({ name: 'skip', required: false, description: 'Number of albums to skip', type: Number })
  async getAlbums(
    @Query('take') take?: string,
    @Query('skip') skip?: string,
  ): Promise<AlbumModel[]> {
    const parsedTake = take ? parseInt(take, 10) : undefined;
    const parsedSkip = skip ? parseInt(skip, 10) : undefined;
    return this.albumService.albums({
      take: parsedTake,
      skip: parsedSkip,
    });
  }

  @Get('albums/count')
  async countAlbums(): Promise<number> {
    return this.albumService.countAlbums({});
  }

  @Get('music/:id')
  async getMusicById(@Param('id') id: string): Promise<MusicModel> {
    return this.musicService.music({ track_uri: String(id) });
  }

  @Get('musics')
  @ApiOperation({ summary: 'Get all musics' })
  @ApiQuery({ name: 'take', required: false, description: 'Number of musics to return', type: Number })
  @ApiQuery({ name: 'skip', required: false, description: 'Number of musics to skip', type: Number })
  async getMusics(
    @Query('take') take?: string,
    @Query('skip') skip?: string,
  ): Promise<MusicModel[]> {
    const parsedTake = take ? parseInt(take, 10) : undefined;
    const parsedSkip = skip ? parseInt(skip, 10) : undefined;
    return this.musicService.musics({
      take: parsedTake,
      skip: parsedSkip,
    });
  }

  @Get('musics/count')
  async countMusics(): Promise<number> {
    return this.musicService.countMusics({});
  }

  @Get('playlist/:id')
  async getPlaylistById(@Param('id') id: string): Promise<PlaylistModel> {
    return this.playlistService.playlist({ playlist_id: Number(id) });
  }

  @Get('playlists')
  @ApiOperation({ summary: 'Get all playlists' })
  @ApiQuery({ name: 'take', required: false, description: 'Number of playlists to return', type: Number })
  @ApiQuery({ name: 'skip', required: false, description: 'Number of playlists to skip', type: Number })
  async getPlaylists(
    @Query('take') take?: string,
    @Query('skip') skip?: string,
  ): Promise<PlaylistModel[]> {
    const parsedTake = take ? parseInt(take, 10) : undefined;
    const parsedSkip = skip ? parseInt(skip, 10) : undefined;
    return this.playlistService.playlists({
      take: parsedTake,
      skip: parsedSkip,
    });
  }

  @Get('playlists/count')
  async countPlaylists(): Promise<number> {
    return this.playlistService.countPlaylists({});
  }

  @Get('music_playlist/:id')
  async getMusic_PlaylistById(@Param('id') id: string): Promise<Music_PlaylistModel> {
    return this.music_playlistService.music_playlist({ music_playlist_id: Number(id) });
  }
  
  @Get('music_playlists/count')
  async countMusic_Playlists(): Promise<number> {
    return this.music_playlistService.countMusic_Playlists({});
  }

}
