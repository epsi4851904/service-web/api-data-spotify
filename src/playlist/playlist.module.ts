import { Module } from '@nestjs/common';
import { PlaylistService } from './playlist.service';
import { PrismaModule } from '../prisma/prisma.module';

@Module({
  imports: [PrismaModule],
  providers: [PlaylistService],
  exports: [PlaylistService],
})
export class PlaylistModule {}