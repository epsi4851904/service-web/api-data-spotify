
import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { Playlist, Prisma } from '@prisma/client';

@Injectable()
export class PlaylistService {
  constructor(private prisma: PrismaService) {}

  async playlist(
    playlistWhereUniqueInput: Prisma.PlaylistWhereUniqueInput,
  ): Promise<Playlist | null> {
    return this.prisma.playlist.findUnique({
      where: playlistWhereUniqueInput,
    });
  }

  async playlists(params: {
    skip?: number;
    take?: number;
    cursor?: Prisma.PlaylistWhereUniqueInput;
    where?: Prisma.PlaylistWhereInput;
    orderBy?: Prisma.PlaylistOrderByWithRelationInput;
  }): Promise<Playlist[]> {
    const { skip, take, cursor, where, orderBy } = params;
    return this.prisma.playlist.findMany({
      skip,
      take,
      cursor,
      where,
      orderBy,
    });
  }

  async countPlaylists(where: Prisma.PlaylistWhereInput): Promise<number> {
    return this.prisma.playlist.count({ where });
  }
  
}
