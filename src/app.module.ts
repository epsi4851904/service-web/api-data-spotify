import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PrismaModule } from './prisma/prisma.module';
import { AlbumModule } from './album/album.module';
import { ArtistModule } from './artist/artist.module';
import { MusicModule } from './music/music.module';
import { MusicPlaylistModule } from './music_playlist/music_playlist.module';
import { PlaylistModule } from './playlist/playlist.module';

@Module({
  imports: [PrismaModule, AlbumModule, ArtistModule, MusicModule, MusicPlaylistModule, PlaylistModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
