
import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { Album, Prisma } from '@prisma/client';

@Injectable()
export class AlbumService {
  constructor(private prisma: PrismaService) {}

  async album(
    albumWhereUniqueInput: Prisma.AlbumWhereUniqueInput,
  ): Promise<Album | null> {
    return this.prisma.album.findUnique({
      where: albumWhereUniqueInput,
    });
  }

  async albums(params: {
    skip?: number;
    take?: number;
    cursor?: Prisma.AlbumWhereUniqueInput;
    where?: Prisma.AlbumWhereInput;
    orderBy?: Prisma.AlbumOrderByWithRelationInput;
  }): Promise<Album[]> {
    const { skip, take, cursor, where, orderBy } = params;
    return this.prisma.album.findMany({
      skip,
      take,
      cursor,
      where,
      orderBy,
    });
  }

  async countAlbums(where: Prisma.AlbumWhereInput): Promise<number> {
    return this.prisma.album.count({ where });
  }
}
