# api-data-spotify

## Comment lancer le projet ?

### Etape 1:

Initialiser la base postgres et importer les données

Suivre les instructions sur le [repository github suivant](https://gitlab.com/epsi4851904/service-web/import-data-spotify)

### Etape 2 :

```
npm install
```

Ajouter le .env à la racine du projet

```
POSTGRES_DB=app
POSTGRES_USER=postgres
POSTGRES_PASSWORD=password
DATABASE_URL="postgresql://postgres:password@localhost:5432/app"
```

Lancer le serveur local
```
npm install
npm start
```